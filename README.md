# The micro closed-loop gas system

This repository contains the code and documentation required to run the control and monitor software for the micro closed loop.
The main application is a node-red program running on a raspberry pi. 

## Documentation
Documentation is stored [locally on this repository](docs/index.md)

## Source code
The `src` folder contains a backup of the image flashed on the Raspberry Pi for backup purposes and the relevant scripts and node-red applications running on the pi. Refer to the [documentation](docs/index.md) to steps are required for initial
configuration of the raspberry pi.

