# Getting started

# Configure a raspberry

The is tested to run on a raspberry Raspbian image. Raspberry 3B+ and 4 were tested. Refer to the [official guide](https://www.raspberrypi.com/documentation/computers/getting-started.html#installing-the-operating-system) to setup the SD card with the operating system for the device

## How to configure the raspberry to run node-red

1. Ensure OS is able to build binary modules
```sh
sudo apt install build-essential git curl
```

2. Download and run the script from github repo to install Node.js, npm and Node-RED
```sh
bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)
```
In the log output copy the address to access Node-RED editor from your personal browser, something like
```sh
http://<hostname>:1880
```
4. Set Node-RED to start on boot
```sh
sudo systemctl enable nodered.service
```
5. Only for the first time, start the Node-RED service manually
```sh
node-red-start
```
If you want to check status of Node-RED:
```sh
sudo systemctl status nodered
```

## Optional: securing node red


### HTTPS

For further information, please refer to the [official guide here](https://nodered.org/docs/user-guide/runtime/securing-node-red).

If your organization is able to provide a signed `.pem` certificate, you can use it to encrypt HTTP traffic and use HTTPS. You can also create a self-signed certicate (note that your browser will complain that the the connection to Node-RED won't be secure as the self-signed certificate is not trusted)

1. Create a private key
```sh
openssl genrsa -out node-key.pem 2048
```
2. Create a certificate request: you will need to fill out a form the most important entry is near the end and is the common name field that should be the FQDN of the server hosting node RED: you can use raspberrypi.home or the device name on the network
```sh
openssl req -new -sha256 -key node-key.pem -out node-csr.pem
```
3. Sign the certificate with the private key to create the self signed certificate
```sh
openssl x509 -req -in node-csr.pem -signkey node-key.pem -out node-cert.pem
```

Edit your default `~/.node-red/settings.js` and change the `https` section:
```js

https: 

    { 
        key: require("fs").readFileSync('pathto/yourprivatekey.pem'), 
        cert: require("fs").readFileSync('pathto/yourcertificate.pem')
    },
    
```


### Admin and password for Node-RED and Node-RED Dashboard

You can also set a username and a password for node RED: un-comment the `adminAuth` property of the settings file adding a user with only read (`read`) or read+edit (`*`) privileges:

```js
adminAuth: {

    type: "credentials",

    users: [

        {
            
            username: "admin",
            password: "somepasswordhere",
            permissions: "*"
        },
        {
            username: "read",
            password: "somepasswordhere",
            permissions: "read"
        }
    ]
}
```

The password are secured by bcrypt algorithm: to hash them run the following command:
```sh
node-red-admin hash-pw
```


To secure also the dashboard use the `httpNodeAuth` property:
```sh
httpNodeAuth: {user:"dashboard_admin",pass:"somepasswordhere"},
```
