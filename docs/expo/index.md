# The micro loop gas systems for the Spark Chambers at CERN's Science Gateway

This documentation is specific for the installation of the micro loop system at the CERN Science Gateway

## Motivations

The spark chambers detector were typically operated by injecting 2-3 ln/h of a gas mixture made of 70% Neon and 30% Helium from a bottle using a rotameter. Due to the price
increase of Neon in the last period, solutions were investigated to reduce the gas consumption.
The goal of the micro loop gas system is to decrease the fresh gas consumption by recirculating the gas and injecting fresh gas from the bottle only when required by the system to operate in a stable manner. Furthermore, the recirculation system is monitoring the humidity level at the input and output of the chamber and uses a
purifier cartridge to remove as much humidity as possible, ensuring a stable operation of the detectors.

The micro loop gas system is a "micro" replica of a recirculating gas system used for particle detectors at CERN LHC.

We estimated that with a micro loop system the gas consumption can be reduced by 95%, increasing the lifetime of a gas bottle and reducing long-term operational costs.

The system is currently installed behind the spark chambers at the Science Gateway:

![img/physical_installation.jpg](img/physical_installation.jpg)

## Drawings

### P&ID

The Process and Instrumentation Diagrams is shown here:

![img/2d_Expo_panel_final.png](img/2d_Expo_panel_final.png)

You can notice the following items

- The gas bottle, a gas mixture of Neon and Helium at $70\%$-$30\%$ ;

- A mass flow controller **F1**  to regulate the flow of fresh gas to be
    injected into the system; The device is calibrated for He with a maximum flow of 2 ln/h ([Vogtlin datasheet](https://www.voegtlin.com/data/329-3028_en_manualsmart110.pdf))

- A **purifier** i.e. a volume with molecular sieve, used to remove
    moisture from the system: the purifier can be by-passed if needed. This allows to change the cartridge without stopping the system

- A 1 um filter, to capture residuals of dust or contaminants;

- A mass flowmeter **F2** which measures the recirculated gas flow
    across a sensing surface using up to 120 L/h. The device is calibrated for Air. ([Renesas FS2012 datasheet](https://www.renesas.com/us/en/document/dst/fs2012-datasheet))

- Two dew point transmitters, one at the input **TD2** and one at
    the output **TD1**. The devices measure the dew point in the range (-80,20)°C, which is an indirect measurement of the amount of water content in the system. ([Vaisala DMT143 datasheet](https://docs.vaisala.com/r/M211435EN-K/en-US/GUID-9CBA94AA-9F34-4BB1-8917-2E80D8237B3A/GUID-276D3AAC-DF64-40B4-96C9-03F61C817ACD))

- A set of safety bubblers inside the chamber mechanical frames to prevent over-pressure in the chambers;

- A controlled **pump** to ricirculate the gas. Its speed is regulated by the control and changes to keep detector's pressure as stable as possible ([Xavitech P1500 datasheet](https://www.xavitech.com/wp-content/uploads/2021/01/BPV1500-Intelligent-Tecnical-datasheet.pdf));

- Two buffer volumes on each side of the pump, to damp pressure
    variations produced by the pump itself and store gas (hence buffer) when atmospheric pressure changes
    
- Two pressure sensors: **PT1** (with range (-10,10) mbarg) which is installed as close as possible to the chamber's output. This pressure is kept as stable as possible by the control system. **PT2** (with range (-150,150) mbarg) Is installed after the pump and provides information about the amount of gas stored in the high-pressure buffer volume.

Externally to the micro loop, but related to the gas system, there is a safety **back-up** mechanism that injects a constant flow of 3 ln/h in case of a powercut. A normally opened valve allows the gas to pass through a rotameter that injects gas directly at the input of the chambers.

### 3D Drawing

The 3D Drawing is shown here:

![img/3d.png](img/3d.png)


## Data acquisition system

The data acquisition happens via a Raspberry Pi computer: the analog
inputs are collected and converted into digital values by an 8 channels
ADC board ([8AI datasheet](https://widgetlords.com/collections/pi-spi-series-1/products/pi-spi-8aiplus-raspberry-pi-analog-input-4-20-ma-interface)) while the analog outputs to control the pump and the mass flow
controller are given by a 2 channels board ([2AO datasheet](https://widgetlords.com/collections/pi-spi-series-1/products/pi-spi-2a0-raspberry-pi-analog-output-ma-vdc-interface)). In addition to this, there is a sensor measuring environmental pressure, temperature and humidity ([Yoctopuce Meteo-V2](https://www.yoctopuce.com/projects/yoctometeoV2/METEOMK2.usermanual-EN.pdf)).

A set of LEDs is installed to display the direction of the flow ([Adafruit NeoPixel guide](https://www.mouser.ch/datasheet/2/737/adafruit_neopixel_uberguide-2488621.pdf)) and controlled directly by the RPi (GPIO 18). Simply applying power to the LEDs will not make them light up: a controller and some programming, as the one from the RPi, is needed.

### Electronics schematics

All the devices are powered at 24V,except for F2 and the LEDs which are powered at 5V. The pump is powered at 12V.
The ports of the RPi used by the two Widgetlord boards are GPIO7 - GPIO4 -GPIO9 - GPIO10 - GPIO11, while the LEDs are using port GPIO18.

| PI-SPI-8AI channel | device                      | range             | name | power supply | notes         |
| ------------------ | --------------------------- | ----------------- | ---- | ------------ | --------------|
|    CH1             |    pressure sensor          | (-150,150) mbarg  | PT2  |  +24V        | loop P        |
|    CH2             |    pressure sensor          | (-10,10) mbarg    | PT1  |  +24V        | det. P        |
|    CH3             |    dew point transmitter    | (-80,20) °C       | TD1  |  +24V        | det. dewpoint |
|    CH4             |    dew point transmitter    | (-80,20) °C       | TD2  |  +24V        | loop dewpoint |
|    CH5             |    mass flow controller     | (0,2) ln/h        | F1   |  +24V        | injected flow |
|    CH6             |    mass flow meter          | (0,120) ln/h      | F2   |  +5V         | loop flow     |
|    CH7             |    N/C                      |                   |      |              |               |
|    CH8             |    N/C                      |                   |      |              |               |

| PI-SPI-2AO channel | pin name  | output     | device            |
| ------------------ | --------- |----------- | ----------------- | 
|    CH1             | A1        | 4..20 mA   | Mass flow control |
|    CH2             | V2        | 0..10 V    | Pump control      |

The maximum ratings of the power supply ([Meanwell RQ85D datasheet](https://docs.rs-online.com/995c/0900766b8136feb3.pdf)), which has a total rated power of 84W, are:

| DC voltage | rated current  |
| ---------- | ---------------|
| +5V        |  6A            |
| +12V       |  2A            |
| +24V       |  1A            |

The power consumptions of the devices are:
| Device | power consumption  |
| ---------- | ---------------|
| Pump       
| Dew point transm.       
| Pressure sensors    
| flowmeter
| MFC   

## Monitor and Control System

The data flow is orchestrated using Node-RED, a low-code programming for event-driven applications with browser-based flow editor. Sensors data is stored into a CERN's InfluxDB instance and displayed on a grafana dashboard ([Grafana dashboard](https://epdt-gasmonitoring.web.cern.ch/d/dorejhGSz/setup-v7?orgId=1)).

The core logic happens in the Node-RED application hosted at https://hostname:1880, where `hostname` is the name of the raspberry device on the network. Each change in the flows or nodes required to re-deploy the flows and will restart the control from initial conditions. 

Some parameters can be toggled and changed without stopping the running logic using the application `dashboard` accessible at https://hostname:1880/ui . The control parameters allows for instance to put the system in auto or manual mode, tune the PID parameters, switch On/Off the LEDs etc.


### Data ingestion 
[TODO: Add screenshot of InfluxDB nodes]

The Node-RED application perform also two independent controls actions: one on the
pump and one on the mass flow controller.

### Pump control 
When in auto mode, the pump speed increases if the **PT1** increases and viceversa. 

[TODO: Screenshot of pump flows]

The pump speed can be also put in manual mode by accessing the dashboard and using the switch "Manual Control"

[TODO: Screenshot of the dashboard]

### Injection control
When in auto mode, the mass flow controller inject fresh gas when the pressure **PT1** decreases below a certain threshold.

[TODO: Screenshot of the flows]

The Mass flow controller can be also put in manual mode by accessing the dashboard 

[TODO: Screenshot of the dashboard ]


# Procedures

## Starting the micro loop gas system

For first time installations or after a shutdown
1. Ensure every pipe is properly connected
2. Plug the Raspberry PI power
3. After about 1 minute, plug the micro loop to the power socket

## Checking that the system is running properly

- Check that the LEDs on the Raspberry PI (either red or green) and on the ADCs board (blues) are ON
- Check that the web app is accessible at https://hostname:1880 and https://hostname:1880/ui
- Check that the switches are set to Auto mode [TODO: Screenshot of dashboard]

## Stop the micro loop gas system

For a graceful stop perform the following actions:
1. Put the pump in manual mode and slowly set it to 0%
2. Put the mass flow controller in manual mode and set it directly to close
[TODO: GIF of the dashboard steps]
If you need to cut the power perform the following additional steps:
3. Shutdown the raspberry: Click on the button to shutdown the raspberry on the dashboard
[TODO: Add node to shutdown rpi: https://flows.nodered.org/node/node-red-contrib-rpi-shutdown]
4. Unplug the micro loop power
5. Unplug the raspberry power


# Troubleshooting



